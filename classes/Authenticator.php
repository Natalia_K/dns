<?php

class Authenticator {

    private $_login;
    private $_password;

    public function __construct(Array $arguments = array())
    {
        if (!empty($arguments)) {
            foreach ($arguments as $property => $argument) {
                $this->{$property} = $argument;
            }
        }
    }

    public function checkAccess()
    {
        if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']))
            throw new Exception('Unauthorized request');
        if ($_SERVER['PHP_AUTH_USER'] != $this->_login || $_SERVER['PHP_AUTH_PW'] != $this->_password)
            throw new Exception('Invalid credentials');

        return ['ip' => $_SERVER['REMOTE_ADDR'], 'agent' => $_SERVER['HTTP_USER_AGENT'], 'timestamp' => time()] ;
    }
}
