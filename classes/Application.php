<?php
require './vendor/autoload.php';
use \Symfony\Component\DomCrawler\Crawler;

require_once("Router.php");
require_once("Parser.php");
require_once("Authenticator.php");

define('WEB_ROOT', dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR);

class Application
{
    protected $_config;
    protected $_db;
    protected $_errors = [];
    protected $_router;
    protected $_auth;
    protected $_downloads;

    public function __construct()
    {
        $this->initialize();
    }

    protected function initialize()
    {
        date_default_timezone_set('Europe/Moscow');

        $dbConfigPath = WEB_ROOT.'config'.DIRECTORY_SEPARATOR.'db.php';
        if (!is_file($dbConfigPath))
            $this->setError('Не найдена конфигурация подключения к БД');

        extract(require($dbConfigPath));

        $dsn = $type.":host=$host;dbname=$db".($type=='mysql'? ";charset=$charset" : '').(!empty($port) ? ';port=$port' : '');
        try {
            $this->_db = new PDO($dsn, $user, $pwd, [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_EMULATE_PREPARES => true
            ]);
        }
        catch (PDOException $e) {
            $this->setError('Подключение не удалось: ' . $e->getMessage());
        }

        $appConfigPath = WEB_ROOT.'config'.DIRECTORY_SEPARATOR.'app.php';
        if (is_file($appConfigPath))
            $this->_config = require($appConfigPath);
        else
            $this->setError('Не найдена конфигурация приложения');

        $this->_downloads = WEB_ROOT.$this->_config['downloadsDir'].DIRECTORY_SEPARATOR;

        if (!is_dir($this->_downloads))
            mkdir($this->_downloads);
        if (!is_dir(WEB_ROOT.'logs'))
            mkdir(WEB_ROOT.'logs');
        if (!is_dir(WEB_ROOT.'cookie'))
            mkdir(WEB_ROOT.'cookie');

        if (php_sapi_name() != 'cli') {
            $this->_auth = new Authenticator(array(
                    '_login' => $this->_config['login'],
                    '_password' => $this->_config['password'])
            );
            $this->_router = new Router(['base' => $this->_config['baseDirectory']]);
        }

    }

    public function getErrors()
    {
        return $this->_errors;
    }

    public function setError($message)
    {
        $this->_errors[] = $message;
    }

    public function getRouter()
    {
        return $this->_router;
    }

    public function baseUrl()
    {
        return $this->_config['baseUrl'];
    }

    public function getParam($name)
    {
        if (isset($_POST[$name]))
            return $_POST[$name];
        elseif(isset($_GET[$name]))
            return $_GET[$name];
        else
            return null;
    }

    public function callAction($name, $params = array(), $prefix = 'action')
    {
        try {
            $this->logAccess($this->_auth->checkAccess());
            $method = $prefix.ucfirst($name);
            if (!method_exists($this, $method))
                throw new Exception('Действие не найдено');
            $response = $this->{$method}($params);
            $this->sendResponse($response);
        }
        catch(Exception $e) {
            $this->setError($e->getMessage());
            $this->sendResponse($this->_errors);
        }

    }

    public function sendResponse($data) {
        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        header("Access-Control-Allow-Origin: ".$this->_config['allowedIPs']);
        echo json_encode($data);
        exit;
    }

    public function logAccess($client) {
        $this->_db->prepare("INSERT INTO access (ip, agent, timestamp) values (?,?,?)")->execute([$client['ip'], $client['agent'], $client['timestamp']]);
    }

    protected function httpQuery($url, $download = false, $filepath = '') {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_COOKIEFILE, WEB_ROOT. "cookie/cookie.txt");
        curl_setopt($ch, CURLOPT_COOKIEJAR, WEB_ROOT. "cookie/cookie.txt");
        curl_setopt($ch, CURLOPT_USERAGENT, 'PostmanRuntime/7.4.0');
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_ENCODING, "gzip, deflate");
        $data = curl_exec($ch);
        curl_close($ch);
        if ($download) {
            $fp = fopen($filepath, "w+");
            fwrite($fp, $data);
            fclose($fp);
        }
        else
            return $data;
    }

    public function actionIndex() {
         $sku = $this->getParam('a');
        $region = $this->getParam('b');
        $product_query = $this->_db->prepare("SELECT products.sku, products.name, products.manufacturer, products.size, products.features, prices.price, prices.bonus
                from products LEFT JOIN prices ON products.sku = prices.product where products.sku = ? and prices.region = ?");
        $product_query->execute([$sku, $region]);
        $product = $product_query->fetch(PDO::FETCH_ASSOC);
        if ($product) $product['features'] = json_decode($product['features']);
        if (!$product) $product = new stdClass();
        return $product;
    }

    public function actionRegions() {
        $regions= $this->_db->query("SELECT code, title from regions")->fetchAll(PDO::FETCH_KEY_PAIR);
        return $regions;
    }

    protected function scrapData($sku)
    {
        $result = [];
        $this->httpQuery($this->_config['infoPath'] . $sku, true, $this->_downloads . $sku . '.html');
        $data = file_get_contents($this->_downloads . $sku . '.html');
        $crawler = new Crawler($data);
        try {
            $result['manufacturer'] = $crawler->filter('[data-product-param="brand"]')->attr('data-value');
            $json = [];
            $crawler->filter('#main-characteristics table tr')->each(function (Crawler $node, $i) use (&$json) {
                $item = [];
                $children = $node->children()->each(function (Crawler $td, $j) {
                    if ($td->attr('colspan') != 2)
                        return $td->text();
                    else
                        return false;
                });
                $children = array_filter($children);
                if (!empty($children) && $children[0]) {
                    $item[$children[0]] = $children[1];
                    $json[] = $item;
                }
            });
            $result['features'] = json_encode($json);
            @unlink($this->_downloads . $sku . '.html');
            return $result;
        } catch (Exception $e) {
            //renew cookies
            @unlink(WEB_ROOT . 'cookie/cookie.txt');
            $this->httpQuery($this->_config['priceUrl'] . 'price-pskov.xls');
            @unlink($this->_downloads . $sku . '.html');
            return [];
        }
    }

    protected function processData($region, $data = []) {
        $existing_products = $this->_db->query("SELECT sku FROM products")->fetchAll(PDO::FETCH_COLUMN);
        $existing_prices = [];
        if (!empty($existing_products)) {
            $in  = str_repeat('?,', count($existing_products) - 1) . '?';
            $values = array_merge($existing_products, [$region]);
            $existing_prices_query = $this->_db->prepare("SELECT id, product from prices where product in ($in) and region = ?");
            $existing_prices_query->execute($values);
            $existing_prices = $existing_prices_query->fetchAll(PDO::FETCH_KEY_PAIR);
        }
        try {
            $this->_db->beginTransaction();
            $current = [];
            foreach ($data as $item) {
                $current = $item;
                if (in_array($item['sku'], $existing_products))
                    $this->_db->prepare("UPDATE products SET name = ?, size = ? where sku = ?")->execute([$item['name'], $item['size'], $item['sku']]);
                else {
                    $this->_db->prepare("INSERT INTO products (sku, name, size) values (?,?,?)")->execute([$item['sku'], $item['name'], $item['size']]);
                    $existing_products[] = $item['sku'];
                }

                if (in_array($item['sku'], $existing_prices)) {
                    $id = array_search($item['sku'], $existing_prices);
                    $this->_db->prepare("UPDATE prices SET updated = ?, price = ?, bonus = ? WHERE id = ?")->execute([time(), $item['price'], $item['bonus'], $id]);
                }
                else
                    $this->_db->prepare("INSERT INTO prices (created, updated, product, price, bonus, region) values (?,?,?,?,?,?)")->execute([time(), time(), $item['sku'], $item['price'], $item['bonus'], $region]);


            }
           $this->_db->commit();
            return true;
        }
        catch (Exception $e){
            $this->_db->rollback();
            $this->setError($e->getMessage());
            $this->log(date('H:i:s').': Ошибка обработки записи '.PHP_EOL.var_export($current, true));
            return false;
        }

    }

    public function batch($r) {
        set_time_limit(0);
        ini_set("memory_limit",-1);
        $region_query = $this->_db->prepare("SELECT code, name from regions where id = ?");
        $region_query->execute([$r]);
        $region = $region_query->fetch(PDO::FETCH_ASSOC);
        if (!empty($region)) {
            $filename = 'price-'.$region['name'].'.xls';
            $filepath = $this->_downloads.$filename;
            if (!file_exists($filepath)) {
                $this->httpQuery($this->_config['priceUrl'].$filename, true, $filepath);
                $this->log(date('H:i:s').': Скачан файл '.$filename);
            }
            else
                $this->log(date('H:i:s').': Начало обработки файла '.$filename);
            $parser = new Parser($filepath);
            $data = $parser->parse();
            if ($this->processData($region['code'], $data)) {
                $this->log(date('H:i:s').': Обработан файл '.$filename);
                @unlink($filepath);
                $r++;
                $command = "php -f index.php ".$r;
                exec($command);
            }
        }
    }

    public function scrap($from, $to) {
        set_time_limit(0);
        ini_set("memory_limit",-1);
        $to = intval($to);
        $from = intval($from);
        $batch_size = $to - $from;
        $products_query = $this->_db->prepare("SELECT * FROM products WHERE features is null LIMIT :limit OFFSET :offset");
        $products_query->bindParam(':limit', $to, PDO::PARAM_INT);
        $products_query->bindParam(':offset', $from, PDO::PARAM_INT);
        $products_query->execute();
        $products = $products_query->fetchAll(PDO::FETCH_ASSOC);
        if ($products) {
            try {
                $this->_db->beginTransaction();
                foreach ($products as $product) {
                    $data = $this->scrapData($product['sku']);
                    if (!empty($data))
                        $this->_db->prepare("UPDATE products SET manufacturer = ?, features = ? where sku = ?")->execute([$data['manufacturer'], $data['features'], $product['sku']]);
                }
                $this->_db->commit();
                $this->log(date('H:i:s').': Обновлена информация с '.$from.' до '.$to);
                $command = "php -f index.php ".($from+$batch_size)." ".($to+$batch_size);
                exec($command);
                exit;
            } catch (Exception $e) {
                $this->_db->rollback();
                $this->log(date('H:i:s').': При обновлении данных что-то пошло не так: '.$e->getMessage());
            }
        }
    }

    protected function log($message) {
        file_put_contents(WEB_ROOT.'logs'.DIRECTORY_SEPARATOR.'log_'.date('d_m_Y').'.txt', $message.PHP_EOL , FILE_APPEND | LOCK_EX);
    }
}