<?php
class Router
{
    private $base;

    public function __construct(Array $arguments = array())
    {
        if (!empty($arguments)) {
            foreach ($arguments as $property => $argument) {
                $this->{$property} = $argument;
            }
        }
    }

    public function parseUrl()
    {
        $params = array();
        $uri = str_replace($this->base, '',$_SERVER['REQUEST_URI']);
        $parts = explode('?', $uri);
        if (!empty($parts[1]))
        {
            $pairs = explode('&', $parts[1]);
            foreach($pairs as $p)
            {
                $pt = explode('=', $p);
                $params[$pt[0]] = $pt[1];
            }

        }
        $segments = explode('/', $parts[0]);
        array_shift($segments);

        $_POST['action'] = (isset($segments[0]) ? $segments[0] : 'index');
        $_POST['params'] = isset($segments[1]) && is_numeric($segments[1]) ? array('id' => $segments[1]) : array();
        if (empty($_POST['action'])) $_POST['action'] = 'index';
        $_POST['params'] = array_merge($_POST['params'], $params);
    }

    public function createUrl($route, $params=array())
    {
        $q = array();
        if (!empty($params))
            foreach ($params as $key => $value)
            {
                $q[] = $key.'='.$value;
            }
        $query = join('&', $q);
        return $this->base.'/'.$route.(!empty($query) ? '?'.$query : '');
    }
}


