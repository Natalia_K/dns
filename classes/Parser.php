<?php
require './vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Xls;

class Parser {

    private $spreadsheet;
    private $config = [];

    public function __construct($path)
    {
        $configPath = dirname(__FILE__).DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'config'.DIRECTORY_SEPARATOR.'parser.php';
        if (!is_file($configPath))
            die('no config');
        $this->config = require($configPath);
        $reader = new Xls();
        $reader->setLoadSheetsOnly($this->config['dataSheets']);
        $reader->setReadDataOnly(true);
        $this->spreadsheet = $reader->load($path);
    }

    public function parse() {
        set_time_limit(0);
        $data = [];
        foreach($this->config['dataSheets'] as $sheetName) {
            $worksheet = $this->spreadsheet->getSheetByName($sheetName);
            $highestRow = $worksheet->getHighestRow();
            $highestColumn = $worksheet->getHighestColumn();
            $letter = substr($highestColumn, -1);
            $beforeHighest = (strlen($highestColumn) > 1 ? substr($highestColumn, 0, -1) : '').chr(ord($letter) - 1);
            $schemaMap = [
                'A' => 'sku',
                'B' => 'name',
                $beforeHighest => 'price',
                $highestColumn => 'bonus'
            ];
            $highestColumn++;
            for ($row = 1; $row <= $highestRow; ++$row) {
                if ($this->checkRow($worksheet, $row, $schemaMap)) {
                    $item = [];
                    for ($col = 'A'; $col != $highestColumn; ++$col) {
                        if (in_array($col, array_keys($schemaMap))) {
                            $cell = $worksheet->getCell($col . $row);
                            $value = $cell->getValue();
                            if (empty($value)) $value = "0";
                            $item[$schemaMap[$col]] = $value;
                        }
                    }
                    $data[] = $item;
                }
            }
            foreach($data as &$item) {
                $item['size'] = '';
                preg_match('/\d+(\.\d+)?[(\'\')|\"]/',$item['name'], $matches);
                if (!empty($matches))
                    $item['size'] = str_replace('"', '', $matches[0]);
            }
        }
        return $data;
    }

    protected function checkRow($worksheet, $row, $schemaMap) {
        $skuCol = array_search('sku', $schemaMap);
        $skuCellValue = $worksheet->getCell($skuCol . $row)->getValue();

        $nameCol = array_search('name', $schemaMap);
        $nameCellValue = $worksheet->getCell($nameCol . $row)->getValue();

        $keywords = join('|', $this->config['productKeys']);

        if (preg_match('/^\d+$/', $skuCellValue) != 1)
            return false;

        if (preg_match("/($keywords)(\s|$)/ui", $nameCellValue) != 1)
            return false;

        return true;
    }



}
