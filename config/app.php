<?php

return [
    'login' => 'user',
    'password' => 'password',
    'baseDirectory' => '/dns',
    'priceUrl' => 'http://dns-shop.ru/files/price/',
    'infoPath' => 'https://dns-shop.ru/search/?q=',
    'downloadsDir' => 'files',
    'allowedIPs' => '*'
];