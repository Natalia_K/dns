--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 10.1

-- Started on 2018-12-04 22:11:27

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 381224)
-- Name: access; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE access (
    id integer NOT NULL,
    ip character varying(20) NOT NULL,
    agent character varying(255) NOT NULL,
    "timestamp" integer NOT NULL
);


--
-- TOC entry 171 (class 1259 OID 381222)
-- Name: access_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE access_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1972 (class 0 OID 0)
-- Dependencies: 171
-- Name: access_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE access_id_seq OWNED BY access.id;


--
-- TOC entry 177 (class 1259 OID 381277)
-- Name: prices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE prices (
    id integer NOT NULL,
    created integer NOT NULL,
    updated integer NOT NULL,
    product integer NOT NULL,
    price real,
    bonus integer,
    region character varying NOT NULL
);


--
-- TOC entry 176 (class 1259 OID 381275)
-- Name: prices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE prices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1973 (class 0 OID 0)
-- Dependencies: 176
-- Name: prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE prices_id_seq OWNED BY prices.id;


--
-- TOC entry 173 (class 1259 OID 381239)
-- Name: products; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE products (
    sku integer NOT NULL,
    name character varying(255) NOT NULL,
    manufacturer character varying(255),
    size character varying(10),
    features json
);


--
-- TOC entry 175 (class 1259 OID 381249)
-- Name: regions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE regions (
    id integer NOT NULL,
    code character varying(4) NOT NULL,
    name character varying(50) NOT NULL,
    title character varying(50) NOT NULL
);


--
-- TOC entry 174 (class 1259 OID 381247)
-- Name: regions_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE regions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 1974 (class 0 OID 0)
-- Dependencies: 174
-- Name: regions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE regions_id_seq OWNED BY regions.id;


--
-- TOC entry 1845 (class 2604 OID 381227)
-- Name: access id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY access ALTER COLUMN id SET DEFAULT nextval('access_id_seq'::regclass);


--
-- TOC entry 1847 (class 2604 OID 381280)
-- Name: prices id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices ALTER COLUMN id SET DEFAULT nextval('prices_id_seq'::regclass);


--
-- TOC entry 1846 (class 2604 OID 381252)
-- Name: regions id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY regions ALTER COLUMN id SET DEFAULT nextval('regions_id_seq'::regclass);


--
-- TOC entry 1849 (class 2606 OID 381229)
-- Name: access access_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY access
    ADD CONSTRAINT access_pkey PRIMARY KEY (id);


--
-- TOC entry 1857 (class 2606 OID 381285)
-- Name: prices prices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT prices_pkey PRIMARY KEY (id);


--
-- TOC entry 1851 (class 2606 OID 381246)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY products
    ADD CONSTRAINT products_pkey PRIMARY KEY (sku);


--
-- TOC entry 1853 (class 2606 OID 381254)
-- Name: regions regions_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);


--
-- TOC entry 1855 (class 2606 OID 381272)
-- Name: regions unique_code; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY regions
    ADD CONSTRAINT unique_code UNIQUE (code);


--
-- TOC entry 1858 (class 2606 OID 381286)
-- Name: prices fk_product_sku; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT fk_product_sku FOREIGN KEY (product) REFERENCES products(sku);


--
-- TOC entry 1859 (class 2606 OID 381291)
-- Name: prices fk_region_code; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY prices
    ADD CONSTRAINT fk_region_code FOREIGN KEY (region) REFERENCES regions(code);


-- Completed on 2018-12-04 22:11:27

--
-- PostgreSQL database dump complete
--

