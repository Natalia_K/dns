<?php
require("classes/Application.php");

$app = new Application;

if (php_sapi_name() == 'cli') {
    if (isset($argv[1]) && isset($argv[2]))
        $app->scrap($argv[1], $argv[2]);
    elseif (isset($argv[1]))
        $app->batch($argv[1]);
}
else {
    $app->getRouter()->parseUrl();

    $action = $app->getParam('action');
    $params = $app->getParam('params');

    $app->callAction($action, $params);
}